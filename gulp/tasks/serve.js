const gulp = require('gulp')

const imageMinify = require('./imageMinify')
const svgSprite = require('./svgSprite')
const styles = require('./styles')
const html = require('./html')
const script = require('./script')
const vendorScript = require('./vendorScript')
const copyDependencies = require('./copyDependencies')
const svg = require('./svg')
const server = require('browser-sync').create()

function readyReload(cb) {
  server.reload()
  cb()
}

module.exports = function serve(cb) {
    server.init({
        server: 'build',
        notify: false,
        open: true,
        cors: true
    })

    gulp.watch('src/img/*.{png,jpg,webp}', gulp.series(imageMinify, readyReload))
    gulp.watch('src/img/svg/*.svg', gulp.series(svg, readyReload))
    gulp.watch('src/img/sprite/*.svg', gulp.series(svgSprite, readyReload))
    gulp.watch('src/styles/**/*.scss', gulp.series(styles, cb => gulp.src('build/css').pipe(server.stream()).on('end', cb)))
    gulp.watch('src/js/**/*.js', gulp.series(vendorScript, readyReload))
    gulp.watch('src/js/**/*.js', gulp.series(script, readyReload))
    gulp.watch('src/html/**/*.html', gulp.series(html, readyReload))
    gulp.watch('src/html/**/*.scss', gulp.series(styles, readyReload))

    gulp.watch('package.json', gulp.series(copyDependencies, readyReload))

    return cb()
}
