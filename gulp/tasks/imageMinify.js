const gulp = require('gulp')
const webp = require('gulp-webp');

module.exports = function imageMinify() {
    return gulp.src('src/img/pics/*.{png,jpg,webp}')
		.pipe(webp({
        quality: 75,
        preset: 'photo',
        method: 6
    }))
		.pipe(gulp.dest('build/img'))
}
