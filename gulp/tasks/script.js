const gulp = require('gulp')
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const gulpFilter = require('gulp-filter');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const sourcemaps = require('gulp-sourcemaps');
const include = require('gulp-include');

module.exports = function script() {
  const customJS = gulpFilter('src/js/custom/**/*.*', {restore: true}),
        vendorJS = gulpFilter('src/js/vendor.js', {restore: true});
  return gulp.src('src/js/**/*.js')
      .pipe(customJS)
        .pipe(sourcemaps.init())
        // .pipe(jshint())
        // .pipe(jshint.reporter(stylish))

         // Get custom JS
        .pipe(babel({
          presets: ['@babel/env']
        }))
        .pipe(gulp.dest('build/js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(customJS.restore)


        // Get vendor JS
        .pipe(vendorJS)
        .pipe(include())
        .pipe(gulp.dest('build/js'))
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('build/js'))
}
