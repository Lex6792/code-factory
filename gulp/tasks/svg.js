const gulp = require('gulp')
const svgo = require('gulp-svgo');


module.exports = function svgSprite() {
  return gulp.src('src/img/svg/*.svg')
    .pipe(svgo())
    .pipe(gulp.dest('build/img/svg'))
}
