const gulp = require('gulp')
const plumber = require('gulp-plumber')
const htmlValidator = require('gulp-w3c-html-validator')
const config = require('../config')
const prettify = require('gulp-html-prettify');
const fileinclude = require('gulp-file-include');
const htmlmin = require('gulp-htmlmin');

module.exports = function html() {
  return gulp.src(['src/html/[^_]*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(plumber())
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(htmlValidator())
    .pipe(gulp.dest('build'));
}
