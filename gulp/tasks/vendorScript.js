const gulp = require('gulp')
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

module.exports = function vendorScript() {
    return gulp.src('src/js/vendor/*.js')
      .pipe(babel({
        presets: ['@babel/env']
      }))
      .pipe(uglify())
      .pipe(concat('vendor.min.js'))
      .pipe(gulp.dest('build/js'))
}
